var db;
var sql='';
var res;
var inId = -1;
var dataset=[];
var sabad = [];
var ex_sql_callback;
var ex_sqlx_callback;
var fileTransfer;
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady(){
	db = window.openDatabase("darma", "1.0", "Darma DB", 1000000);
	db.transaction(createTable, errorCB, successCB);
	document.addEventListener("backbutton", onBackKeyDown, false);
	fileTransfer = new FileTransfer();
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	document.addEventListener("menubutton", menuButtonClick, false);
	$( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
		alert('خطا در ارتباط با شبکه'+"\n"+jqxhr.responseText+"\n"+settings.url);
	});
	intial();
}
function download(uri,fn)
{
	var fileName = uri.split('/')[uri.split('/').length-1];
	var filePath = 'cdvfile://localhost/persistent/'+mainDirectory+'/'+fileName;
	fileTransfer.download(
	    uri,
	    filePath,
	    function(entry) {
		if(typeof fn == 'function')
			fn(fileName);
	    },
	    function(error) {
	    }
	);
	
}
var fn_exists;
function file_exists(file_name,fn)
{
	fn_exists = fn
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
		fs.root.getFile(mainDirectory+'/'+file_name, null, function(fileEntery){
			//fn(true);
			fileEntery.file(function(fileObj){
				//alert('Exists');
				fn_exists(true);
/*
				var reader = new FileReader();
				reader.onloadend = function(evt) {
					alert("Read as data URL");
					alert(evt.target.result);
				};
				reader.readAsDataURL(fileObj);
*/
			},fail3);
		}, fail2);
	}, fail1);
//	var fileSource = 'cdvfile://localhost/persistent/'+mainDirectory+'/'+file_name;
//	alert('checking '+file_name);
/*
	reader.onloadend = function(evt) {

	    if(evt.target.result == null) {
	       // If you receive a null value the file doesn't exists
		alert('exits .');
		fn(true);
	    } else {
		// Otherwise the file exists
		alert('not exists!');
		fn(false);
	    }         
	};
	alert('check');
	// We are going to check if the file exists
	reader.readAsDataURL(fileSource);
*/
	//window.resolveLocalFileSystemURL(fileSource,fn(true),fn(false));
}
function install_file(fileName)
{
	var filePath = 'cdvfile://localhost/persistent/'+mainDirectory+'/'+fileName;
        window.plugins.webintent.startActivity({
            action: window.plugins.webintent.ACTION_VIEW,
            url: filePath,
            type: 'application/vnd.android.package-archive'
            },
            function(){
	    },
            function(e){
                alert('Error launching app update');
            }
        );
	navigator.app.exitApp();
}
function gotFS(fileSystem) {
	fileSystem.root.getDirectory(mainDirectory, {create: true}, gotDir);
}

function fail(err)
{
	//alert("0:"+err.code);
}
function fail1(err)
{
	//alert("1:"+err.code);
}
function fail2(err)
{
	//alert("2:"+err.code);
	fn_exists(false);
}
function fail3(err)
{
	//alert("3:"+err.code);
}

function gotDir(dirEntry) {
}
function createTable(tx)
{
	for(var i = 0;i < intialQuery.length;i++)
		tx.executeSql(intialQuery[i]);
}
function populateDBIn(tx) {
	if(sql!='')
	{
		tx.executeSql(sql);
		if(typeof ex_sqlx_callback=='function')
	                ex_sqlx_callback();
	}
}
function populateDBInOut(tx) {
        if(sql!='')
	{
                tx.executeSql(sql,[], querySuccess, errorCB);
	}
}
function querySuccess(tx, results) {
	dataset = [];
	inId = -1;
	res = results;
	rowss = res.rows;
	for(var i = 0;i < rowss.length; i++)
	{
		var row = rowss.item(i);
		dataset.push(row);
	}
	try{
                if(res.rowsAffected>0)
                        inId = res['insertId'];
        }catch(e){
        }
	if(typeof ex_sql_callback=='function')
		ex_sql_callback(dataset,inId);
}
function ex_sqlx(sqlIn,fn)
{
	sql = sqlIn;
	ex_sqlx_callback = fn;
	db.transaction(populateDBIn, errorCB, successCB);
}
function ex_sql(sqlIn,fn)
{
	sql = sqlIn;
	ex_sql_callback = fn;
	db.transaction(populateDBInOut, errorCB, successCB);
}
function errorCB(err) {
    alert("Error processing SQL: "+err.message);
    return false;
}

function successCB() {
}
var tmp_counter = 0;
var tmp_data;
//-----------------------------------brand---------------------------------
function updateBrand()
{
	$.getJSON(server_url+"android/brand.php",function(result){
		ex_sql("DROP TABLE IF EXISTS brand",function(indataset,inInId){
			ex_sql("CREATE TABLE IF NOT EXISTS brand (id integer, name text, pic text)",function(indataset,inInId){	
				tmp_counter = 0;
				tmp_data = result;
				brandInsert();
			});
		});
	});
}
function brandInsert()
{
	ex_sql('INSERT INTO brand (id,name,pic) VALUES ('+tmp_data[tmp_counter].id+',"'+tmp_data[tmp_counter].name+'","'+tmp_data[tmp_counter].pic+'")',function(indataset,inInId){
		tmp_counter++;
		if(tmp_counter<tmp_data.length)
			brandInsert();
		else
		{
			alert('بروزرسانی با موفقیت انجام شد');
			menuButtonClick();
			drawAbarKala();
		}
	});
}
//-----------------------------------abar_group----------------------------
function updateKalaAbarGroup()
{
	$.getJSON(server_url+"android/kala_abar_group.php",function(result){
		ex_sql("DROP TABLE IF EXISTS kala_abarGroup",function(indataset,inInId){
			ex_sql("CREATE TABLE IF NOT EXISTS kala_abarGroup (id integer, name text)",function(indataset,inInId){	
				tmp_counter = 0;
				tmp_data = result;
				kalaAbarGroupInsert();
			});
		});
	});
}
function kalaAbarGroupInsert()
{
	ex_sql('INSERT INTO kala_abarGroup (id,name) VALUES ('+tmp_data[tmp_counter].id+',"'+tmp_data[tmp_counter].name+'")',function(indataset,inInId){
		tmp_counter++;
		if(tmp_counter<tmp_data.length)
			kalaAbarGroupInsert();
		else
		{
			updateKalaGroup();
		}
	});
}
//-----------------------------------group----------------------------
function updateKalaGroup()
{
        $.getJSON(server_url+"android/kala_group.php",function(result){
                ex_sql("DROP TABLE IF EXISTS kala_group",function(indataset,inInId){
                        ex_sql("CREATE TABLE IF NOT EXISTS kala_group (id integer, name text,kala_abarGroup_id integer)",function(indataset,inInId){
                                tmp_counter = 0;
                                tmp_data = result;
                                kalaGroupInsert();
                        });
                });
        });
}
function kalaGroupInsert()
{
        ex_sql('INSERT INTO kala_group (id,name,kala_abarGroup_id) VALUES ('+tmp_data[tmp_counter].id+',"'+tmp_data[tmp_counter].name+'",'+tmp_data[tmp_counter].kala_abarGroup_id+')',function(indataset,inInId){
                tmp_counter++;
                if(tmp_counter<tmp_data.length)
                        kalaGroupInsert();
		else
		{
			updateKalaMiniGroup();
		}
        });
}
//-----------------------------------mini_group----------------------------
function updateKalaMiniGroup()
{
        $.getJSON(server_url+"android/kala_mini_group.php",function(result){
                ex_sql("DROP TABLE IF EXISTS kala_miniGroup",function(indataset,inInId){
                        ex_sql("CREATE TABLE IF NOT EXISTS kala_miniGroup (id integer, name text,kala_group_id integer)",function(indataset,inInId){
                                tmp_counter = 0;
                                tmp_data = result;
                                kalaMiniGroupInsert();
                        });
                });
        });
}
function kalaMiniGroupInsert()
{
        ex_sql('INSERT INTO kala_miniGroup (id,name,kala_group_id) VALUES ('+tmp_data[tmp_counter].id+',"'+tmp_data[tmp_counter].name+'",'+tmp_data[tmp_counter].kala_group_id+')',function(indataset,inInId){
                tmp_counter++;
                if(tmp_counter<tmp_data.length)
                        kalaMiniGroupInsert();
		else
		{
			updateKala();
		}
        });
}
//-----------------------------------kala----------------------------------
function updateKala()
{
        $.getJSON(server_url+"android/kala.php",function(result){
                ex_sql("DROP TABLE IF EXISTS kala",function(indataset,inInId){
                        ex_sql("CREATE TABLE IF NOT EXISTS kala (id integer, name text,kala_miniGroup_id integer,brand_id integer,ghimat integer,ghimat_user integer,pic text)",function(indataset,inInId){
                                tmp_counter = 0;
                                tmp_data = result;
                                kalaInsert();
                        });
                });
        });
}
function kalaInsert()
{
	//for(i in tmp_data[tmp_counter])
		//alert(i+" => "+tmp_data[tmp_counter][i]);
        ex_sql('INSERT INTO kala (id,name,kala_miniGroup_id,brand_id,ghimat,ghimat_user,pic) VALUES ('+tmp_data[tmp_counter].id+',"'+tmp_data[tmp_counter].name+'",'+tmp_data[tmp_counter].kala_miniGroup_id+','+tmp_data[tmp_counter].brand_id+','+tmp_data[tmp_counter].ghimat+','+tmp_data[tmp_counter].ghimat_user+',"'+tmp_data[tmp_counter].pic+'")',function(indataset,inInId){
                tmp_counter++;
                if(tmp_counter<tmp_data.length)
                        kalaInsert();
		else
		{
			updateBrand();
		}
        });
}
//--------------------------------------------------------------------------
function addLog(inp)
{
	$("#log").append(inp+"<br/>");
}
function onBackKeyDown()
{
	back_page();
}
function menuButtonClick()
{
	$(".navbar-toggle").click();
}
function getConf(fn)
{
	ex_sql("select * from conf",function(d,a){
		fn(d);
	});
}
function saveConf(user,pass,addr)
{
	ex_sql("delete from conf",function(a,b){
		ex_sql("insert into conf (user,pass,addr) values ('"+user+"','"+pass+"','"+addr+"')",function(a,b){
		});
	});
}
function addToSabad(kala_id,tedad)
{
	var index = -1;
	newKala(kala_id,function(kala_tmp){
		for(var i = 0;i < sabad.length;i++)
			if(kala_id == sabad[i].kala_id)
				index = i;
		if(index>=0)
			sabad[index].tedad += tedad;
		else
			sabad.push({"kala_id":kala_id,"tedad":tedad,"kala":kala_tmp[0]});
	});
}
var sabad_index = 0;
function addFactorDet(idd)
{
	var i = sabad_index;
	ex_sql("insert into factor_det (kala_id,kala_name,ghimat,tedad,factor_id) values ("+sabad[i].kala_id+",'"+sabad[i].kala.name+"',"+sabad[i].kala.ghimat+","+sabad[i].tedad+","+idd+")",function(a,b){
		if(b <= 0)
			alert('Error factor_det saveing : '+"insert into factor_det (kala_id,kala_name,ghimat,tedad,factor_id) values ("+sabad[i].kala_id+",'"+sabad[i].kala.name+"',"+sabad[i].kala.ghimat+","+sabad[i].tedad+","+idd+")");
		else
		{
			sabad_index++;
			if(sabad.length>sabad_index)
				addFactorDet(idd);
			else
			{
				alert("factor save done");
				sabad = [];
			}
		}
	});
}
function addFactor(factor_shomare,tarikh,ghimat,ersal)
{
	sabad_index = 0;
	ex_sql("insert into factor (factor_shomare,tarikh,ghimat,ersal) values ("+factor_shomare+",'"+tarikh+"',"+ghimat+","+ersal+")",function(d,idd){
		if(idd>0)
			addFactorDet(idd);
		else
			alert('Error factor saveing : '+"insert into factor (factor_shomare,tarikh,ghimat,ersal) values ("+factor_shomare+",'"+tarikh+"',"+ghimat+","+ersal+")");
	});
}
function setLandscape()
{
	try{
		screen.lockOrientation("landscape");
	}catch(e){
		alert(e.message);
	}
}
function disableLandscape()
{
	try{
		screen.unlockOrientation();
	}catch(e){
		alert(e.message);
	}
}
//--------------------------------------TEST---------------------------------
function outputDB()
{
	ex_sql('SELECT * FROM kala',function(indataset,inInId){
		for(var i = 0;i < indataset.length;i++)
			for(j in indataset[i])
				alert(j+':'+indataset[i][j]);
	});
}
function populateDB()
{
	updateKalaAbarGroup();
}
function externalOpen(url)
{
	try{
		ChromeLauncher.open(url);
	}catch(e){
		alert(e.message);
	}
}
